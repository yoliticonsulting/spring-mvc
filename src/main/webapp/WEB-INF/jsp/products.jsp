<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
	<section class="container">
		<div class="row">
			<c:forEach items="${products}" var="product">
				<div class="card col-sm-6 col-md-3  mr-2" style="width: 18rem;">
					<img src="<c:url value="/img/${product.productId}.png"> </c:url>" alt="image" class="card-img-top" />
					<div class="card-body">
						<h5 class="card-title">${product.name}</h5>
						<p class="card-text">${product.description}</p>
						<p class="card-text">${product.unitPrice}USD</p>
						<p class="card-text">Available ${product.unitsInStock} units
							in stock</p>
						<a href="<spring:url value="/product?id=${product.productId}" />" class="btn btn-primary"> 
							<span class="glyphicon-info-sign glyphicon"/></span> Details </a>
					</div>
				</div>
			</c:forEach>
		</div>
	</section>
