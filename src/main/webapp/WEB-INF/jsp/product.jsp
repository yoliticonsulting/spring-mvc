<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

	<section class="container" ng-app="cartApp">
		<div class="row" ng-controller="cartCtrl">
			<div class="col-md-6">
				<img src="<c:url value="/img/${product.productId}.png"> </c:url>" alt="image" class="img-fluid"/>
			</div>
			<div class="col-md-6">
				<h3>${product.name}</h3>
				<p>${product.description}</p>
				<p>
					<strong>Item Code : </strong><span class="badge badge-warning">${product.productId}</span>
					
				</p>
				<p>
					<strong>manufacturer</strong> : ${product.manufacturer}
				</p>
				<p>
					<strong>category</strong> : ${product.category}
				</p>
				<p>
					<strong>Available units in stock </strong> : ${product.unitsInStock}
				</p>
				<h4>${product.unitPrice}USD</h4>
				<p>
					<a href="#" class="btn btn-warning btn-large" ng-click="addToCart('${product.productId}')"> 
					   <span class="glyphicon-shopping-cart glyphicon"> </span> Order Now
					</a>
					<a href="<c:url value="/cart" />" class="btn btn-primary">
				       <span class="glyphicon-hand-right glyphicon"></span> View Cart
				    </a>
									
				</p>
			</div>
		</div>
	</section>
