<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
	<a class="navbar-brand" href="#">Web Store</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item "><a class="nav-link"
				href="<spring:url value="/products"/>">Home </a></li>
			<li class="nav-item "><a class="nav-link"
				href="<spring:url value="/products/"/>">Products </a></li>
			<li class="nav-item "><a class="nav-link"
				href="<spring:url value="/products/add"/>">Add Product </a></li>
			<li class="nav-item "><a class="nav-link"
				href="<spring:url value="/cart/"/>">Cart </a></li>
		</ul>

		<div class="float-right pr-2">
			<a href="?lang=en">English</a>|<a href="?lang=es">Espa&ntilde;ol</a>
			|<a href="<c:url value="/logout" />">Logout</a>
		</div>

	</div>
</nav>
