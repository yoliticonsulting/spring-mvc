<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset="utf-8">
<link rel="stylesheet"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<title>Invalid cart</title>
</head>
<body>
    <section>
        <div class="jumbotron">
            <div class="container">
                <h1 class="alert alert-danger">Customer data is missing</h1>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <p>
            <form:form modelAttibute="order">
                <input id="btnCancel" class="btn btn-primary" type="submit"
                            name="_eventId_backToCustomer" value="Regresar" />
            </form:form>
        </div>
    </section>
</body>
</html>