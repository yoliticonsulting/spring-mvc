var cartApp = angular.module('cartApp', []);

cartApp.controller('cartCtrl', function($scope, $http) {

    $scope.refreshCart = function(cartId) {
        $http.get('/tienda-1/rest/cart/' + $scope.cartId)
            .then(function(resp) {
                $scope.cart = resp.data;
            });
    };

    $scope.clearCart = function() {
        $http.delete('/tienda-1/rest/cart/' + $scope.cartId)
            .then(function(data) {
                $scope.refreshCart($scope.cartId);
            });

    };

    $scope.initCartId = function(cartId) {
        $scope.cartId = cartId;
        $scope.refreshCart($scope.cartId);
    };

    $scope.addToCart = function(productId) {
        $http.put('/tienda-1/rest/cart/add/' + productId)
            .then(function(data) {
                alert("Product Successfully added to the Cart!");
            });
    };
    $scope.removeFromCart = function(productId) {
        $http.put('/tienda-1/rest/cart/remove/' + productId)
            .then(function(data) {
                $scope.refreshCart($scope.cartId);
            });
    };
});
