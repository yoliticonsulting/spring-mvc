package mx.curso.proyecto.tienda.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import mx.curso.proyecto.tienda.domain.Product;
import mx.curso.proyecto.tienda.exceptions.ProductNotFoundException;
import mx.curso.proyecto.tienda.service.ProductService;

@Slf4j
public class ProductIdValidator implements ConstraintValidator<ProductId, String> {
	@Autowired
	private ProductService productService;

	public boolean isValid(String value, ConstraintValidatorContext context) {
		Product product;
		try {
			product = productService.getProductById(value);
		} catch (ProductNotFoundException e) {
			log.error(e.getMessage(),e);
			return true;
		}
		if (product != null) {
			return false;
		}
		return true;
	}
}
