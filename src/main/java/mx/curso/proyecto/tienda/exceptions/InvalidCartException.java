package mx.curso.proyecto.tienda.exceptions;

public class InvalidCartException extends RuntimeException {

	private static final long serialVersionUID = -6466229411897502670L;
	private String cartId;

	public InvalidCartException(String cartId) {
		this.cartId = cartId;
	}

	public String getCartId() {
		return cartId;
	}
}
