package mx.curso.proyecto.tienda.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("")
public class WelcomeController {

	@RequestMapping(value = { "/", "/index" })
	public String welcome(Model model, RedirectAttributes redirectAttributes) {
		model.addAttribute("greeting", "Bienvenido a la tienda!");
		model.addAttribute("tagline", "Una tienda muy sencilla.");
		redirectAttributes.addFlashAttribute("greeting", "Bienvenido a la tienda!");
		redirectAttributes.addFlashAttribute("tagline", "Una tienda muy sencilla.");
		return "redirect:/welcome/greeting";

	}

	@RequestMapping("/welcome/greeting")
	public String greeting() {
		return "welcome";
	}
}
