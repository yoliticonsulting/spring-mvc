package mx.curso.proyecto.tienda.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Controller("/error")
public class ErrorController {
	@ExceptionHandler(Exception.class)
	public String handleError(Model model, Exception ex) {

		model.addAttribute("exception", ex);

		return "error";
	}
}
