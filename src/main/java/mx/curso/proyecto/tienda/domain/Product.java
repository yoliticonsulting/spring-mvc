package mx.curso.proyecto.tienda.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import mx.curso.proyecto.tienda.validator.ProductId;

@Data
@EqualsAndHashCode(exclude = {"unitPrice","description","manufacturer","category","unitsInStock","unitsInOrder"})
@XmlRootElement
public class Product implements Serializable {

	private static final long serialVersionUID = 8716867243110608024L;

	@Pattern(regexp="P[1-9]+", message="{pattern.product.productId.validation}")
	@ProductId
	private String productId;
	
	@Size(min=4, max=50, message="{size.product.name.validation}")
	private String name;
	
	@Min(value=0, message="{min.product.unitPrice.validation}")
	@Digits(integer=8, fraction=2, message="{digits.product.unitPrice.validation}")
	@NotNull(message= "{notNull.product.unitPrice.validation}")
	private BigDecimal unitPrice;
	private String description;
	private String manufacturer;
	private String category;
	private long unitsInStock;
	private long unitsInOrder;
	private boolean discontinued;
	private String condition;	
	private MultipartFile productImage;
	
	@JsonIgnore
	@XmlTransient
	public MultipartFile getProductImage() {
		return this.productImage;
	}
	
	public Product() {
		super();
	}

	public Product(String productId, String name, BigDecimal unitPrice) {
		this.productId = productId;
		this.name = name;
		this.unitPrice = unitPrice;
	}

	
}
