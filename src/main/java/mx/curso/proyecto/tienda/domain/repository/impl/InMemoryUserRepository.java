package mx.curso.proyecto.tienda.domain.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import mx.curso.proyecto.tienda.domain.User;
import mx.curso.proyecto.tienda.domain.repository.UserRepository;

@Repository
public class InMemoryUserRepository implements UserRepository {
	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	private static final class UserMapper implements RowMapper<User> {
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			User user = new User();
			user.setUsername(rs.getString("USERNAME"));
			user.setPassword(rs.getString("PASSWORD"));
			return user;
		}
	}

	@Override
	public User findByUsername(String username) {
		String SQL = "SELECT * FROM USERS WHERE USERNAME = :username";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("username", username);
		try {
			return jdbcTemplate.queryForObject(SQL, params, new UserMapper());
		} catch (DataAccessException e) {
			throw new UsernameNotFoundException(username);
		}
	}
}
