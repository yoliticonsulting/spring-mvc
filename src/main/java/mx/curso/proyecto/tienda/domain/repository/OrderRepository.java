package mx.curso.proyecto.tienda.domain.repository;

import mx.curso.proyecto.tienda.domain.Order;

public interface OrderRepository {
	long saveOrder(Order order);
}
