package mx.curso.proyecto.tienda.domain.repository;

import mx.curso.proyecto.tienda.domain.Cart;
import mx.curso.proyecto.tienda.dto.CartDto;

public interface CartRepository {
	void create(CartDto cartDto);
    Cart read(String id);
    void update(String id, CartDto cartDto);
    void delete(String id);
    void addItem(String cartId, String productId);
    void removeItem(String cartId, String productId);
    
    void clearCart(String cartId);
}
