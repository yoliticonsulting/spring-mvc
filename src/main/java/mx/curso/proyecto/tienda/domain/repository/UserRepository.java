package mx.curso.proyecto.tienda.domain.repository;

import mx.curso.proyecto.tienda.domain.User;

public interface UserRepository {
	User findByUsername(String username);
}
