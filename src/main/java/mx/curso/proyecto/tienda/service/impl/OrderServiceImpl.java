package mx.curso.proyecto.tienda.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.curso.proyecto.tienda.domain.Order;
import mx.curso.proyecto.tienda.domain.repository.OrderRepository;
import mx.curso.proyecto.tienda.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	@Autowired
	private OrderRepository orderRepository;

	@Override
	public Long saveOrder(Order order) {
		return orderRepository.saveOrder(order);
	}
}
