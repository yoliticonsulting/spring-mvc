package mx.curso.proyecto.tienda.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.curso.proyecto.tienda.domain.User;
import mx.curso.proyecto.tienda.domain.repository.UserRepository;
import mx.curso.proyecto.tienda.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository repository;
	
	@Override
	public User findByUsername(String username) {
		return repository.findByUsername(username);
	}

}
