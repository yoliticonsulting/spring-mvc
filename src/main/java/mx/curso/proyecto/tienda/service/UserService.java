package mx.curso.proyecto.tienda.service;

import mx.curso.proyecto.tienda.domain.User;

public interface UserService {
	User findByUsername(String username);
}
