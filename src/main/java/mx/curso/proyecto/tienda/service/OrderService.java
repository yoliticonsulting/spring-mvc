package mx.curso.proyecto.tienda.service;

import mx.curso.proyecto.tienda.domain.Order;

public interface OrderService {
	Long saveOrder(Order order);
}
