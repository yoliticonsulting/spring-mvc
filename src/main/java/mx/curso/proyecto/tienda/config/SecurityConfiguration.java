package mx.curso.proyecto.tienda.config;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import mx.curso.proyecto.tienda.domain.User;
import mx.curso.proyecto.tienda.service.UserService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Autowired
	private UserService userService;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

		auth.inMemoryAuthentication().withUser("john").password(encoder.encode("pa55word")).roles("USER");
		auth.inMemoryAuthentication().withUser("admin").password(encoder.encode("root123")).roles("USER", "ADMIN");
		auth.authenticationProvider(bddUserDetailService());

	}

	private AuthenticationProvider bddUserDetailService() {

		return new AuthenticationProvider() {
			
			@Override
			public boolean supports(Class<?> authentication) {
				return authentication.equals(UsernamePasswordAuthenticationToken.class);
			}
			
			@Override
			public Authentication authenticate(Authentication authentication) throws AuthenticationException {
				String username = authentication.getName();
				String password = authentication.getCredentials().toString();
				User usr = userService.findByUsername(username);
				if (usr!=null && password.equals(usr.getPassword())) {
					List<GrantedAuthority> lst = new ArrayList<GrantedAuthority>();
					lst.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
					return new UsernamePasswordAuthenticationToken(username, password, lst);
				} else {
					return null;
				}
			}
		};
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.formLogin().loginPage("/login").usernameParameter("userId").passwordParameter("password");
		http.formLogin().defaultSuccessUrl("/products/add").failureUrl("/login?error");
		http.logout().logoutSuccessUrl("/login?logout");
		http.exceptionHandling().accessDeniedPage("/login?accessDenied");
		http.authorizeRequests().antMatchers("/**/add").access("hasRole('ADMIN')").antMatchers("/**").permitAll();
		http.csrf().disable();
	}
}
