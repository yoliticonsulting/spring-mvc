package mx.curso.proyecto.tienda.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class WelcomeControllerTest {
	@Autowired
	private MockMvc mvc;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testWelcome() throws Exception {
		this.mvc.perform(get("/")).andExpect(status().is3xxRedirection()).andDo(print());
	}

	@Test
	public void testGreeting() throws Exception {
		this.mvc.perform(get("/welcome/greeting")).andExpect(status().isOk()).andDo(print());
	}

}
