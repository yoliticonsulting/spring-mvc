package mx.curso.proyecto.tienda.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import mx.curso.proyecto.tienda.domain.Product;
import mx.curso.proyecto.tienda.exceptions.ProductNotFoundException;
import mx.curso.proyecto.tienda.service.ProductService;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private ProductService productService;

	@Before
	public void setUp() throws Exception {
		when(productService.getProductById("P1234"))
				.thenReturn(new Product("P1234", "iPhone testing", BigDecimal.valueOf(300)));

		when(productService.getProductById("P7890")).thenThrow(new ProductNotFoundException("P7890"));
	}

	@Test
	public void testGetProductByIdFoundHtmlFormat() throws Exception {
		mvc.perform(get("/product").param("id", "P1234").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(forwardedUrl("/WEB-INF/layouts/template/baseLayout.jsp"))
			.andExpect(model().attribute("product", new Product("P1234", "iPhone testing", BigDecimal.valueOf(300))))
			;
		
	}

	@Test
	public void testGetProductByIdFoundJsonFormat() throws Exception {
		mvc.perform(get("/product.json").param("id", "P1234").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.product.name", is("iPhone testing")));
	}

	@Test
	public void testGetProductByIdNotFoundJsonFormat() throws Exception {
		mvc.perform(get("/product").param("id", "P8907"))
			.andExpect(status().isOk());
	}

}
