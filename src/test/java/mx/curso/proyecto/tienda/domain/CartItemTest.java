package mx.curso.proyecto.tienda.domain;

import java.math.BigDecimal;

import org.junit.*;

public class CartItemTest {
	private CartItem cartItem;

	@Before
	public void setup() {
		cartItem = new CartItem("1");
	}

	@Test
	public void cartItem_total_price_should_be_equal_to_product_unit_price_in_case_of_single_quantity() {
		// Arrange
		Product iphone = new Product("P1234", "iPhone 5s", new BigDecimal(500));
		cartItem.setProduct(iphone);
		// Act
		BigDecimal totalPrice = cartItem.getTotalPrice();
		// Assert
		Assert.assertEquals(iphone.getUnitPrice(), totalPrice);
	}
	
	@Test
	public void update_cartitem_quantity_should_update_total_price() {
		// Arrange
		Product iphone = new Product("P1234", "iPhone 5s", new BigDecimal(600));
		cartItem.setProduct(iphone);
		cartItem.setQuantity(3);
		// Act
		BigDecimal totalPrice = cartItem.getTotalPrice();
		// Assert
		Assert.assertEquals(BigDecimal.valueOf(1800), totalPrice);
		
	}
	
}
