package mx.curso.proyecto.tienda.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class CartTest {
	@Before
	public void setUp() {
		
	}
	
	private Cart initCart() {
		Cart cart = new Cart("1234");
		return cart;
	}
	
	@Test
	public void cart_is_empty_when_new() {
		// Arrange
		Cart cart = initCart();
		
		// Act
		// Assert
		assertTrue(cart.getCartItems().isEmpty());
		
	}
	
	@Test
	public void cart_has_2_product_price_500_total_is_1000() {
		Cart cart = initCart();
		CartItem item1 = new CartItem("1");
		item1.setProduct(new Product("P1234","Apple iphone",BigDecimal.valueOf(500.0)));
		CartItem item2 = new CartItem("2");
		item2.setProduct(new Product("P1235","Laptop",BigDecimal.valueOf(500.0)));
		cart.getCartItems().add(item1);
		cart.getCartItems().add(item2);
		
		
		// ACT
		BigDecimal total = cart.getGrandTotal();

		// Assert
		assertEquals(BigDecimal.valueOf(1000.0), total);
	}
	
	@Test
	public void cart_has_2_product_price_500_and_remove_one_total_is_500() {
		Cart cart = initCart();
		CartItem item1 = new CartItem("1");
		item1.setProduct(new Product("P1234","Apple iphone",BigDecimal.valueOf(500.0)));
		CartItem item2 = new CartItem("2");
		item2.setProduct(new Product("P1235","Laptop",BigDecimal.valueOf(500.0)));
		cart.getCartItems().add(item1);
		cart.getCartItems().add(item2);
		
		cart.getCartItems().remove(item2);
		
		// ACT
		BigDecimal total = cart.getGrandTotal();

		// Assert
		assertEquals(BigDecimal.valueOf(500.0), total);
	}
}
