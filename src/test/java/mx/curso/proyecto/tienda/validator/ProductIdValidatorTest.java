package mx.curso.proyecto.tienda.validator;


import java.math.BigDecimal;

import javax.validation.ConstraintViolation;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import mx.curso.proyecto.tienda.domain.Product;
import mx.curso.proyecto.tienda.service.ProductService;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;
import javax.validation.Validator;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductIdValidatorTest {

	@MockBean
	private ProductService service;
	
	@Autowired
	private Validator validator;

	@Test
	public void product_already_exists() {
		// Arrange
		Product product = new Product("P1234", "Iphone", BigDecimal.valueOf(500.0));
		when(service.getProductById("P1234")).thenReturn(product);
		// Act
		Set<ConstraintViolation<Product>> constrints =  validator.validate(product);
		// Assert
		assertFalse(constrints.isEmpty());
	}

}
