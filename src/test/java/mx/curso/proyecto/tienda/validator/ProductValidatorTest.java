package mx.curso.proyecto.tienda.validator;


import java.math.BigDecimal;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.ValidationUtils;

import mx.curso.proyecto.tienda.domain.Product;
import mx.curso.proyecto.tienda.service.ProductService;

import static org.mockito.Mockito.when;
import org.springframework.validation.BindException;
import javax.validation.Validation;
import javax.validation.ValidationException;
import javax.validation.Validator;
import org.junit.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProductValidatorTest {
	@Autowired
	private UnitsInStockValidator unitsValidator;
	
	@MockBean
	private ProductService productServiceImpl;
	
	private Validator validator;

	@Before
	public void start() {
		this.validator=Validation.buildDefaultValidatorFactory().getValidator();
		when(productServiceImpl.getProductById("P1234")).thenReturn(new Product("P1234", "iPhone 5s", new BigDecimal(500)));
	}
	
	@Test(expected = ValidationException.class)
	public void product_without_UnitPrice_should_be_invalid() {
		// Arrange
		Product product = new Product();
		BindException bindException = new BindException(product, "product");
		//Act
		validator.validate(product);
		//Assert
		Assert.assertEquals(1, bindException.getErrorCount());
		Assert.assertTrue(bindException.getLocalizedMessage().contains("Unit price is Invalid. It cannot be empty."));
	}
	@Test(expected = ValidationException.class)
	public void add_more_than_99_units_if_unit_price_is_greater_than_1000() {
		// Arrange
		Product product = new Product("P1237","Test Mock", new BigDecimal(2000));
		product.setUnitsInStock(100);
		BindException bindException = new BindException(product, "product");
		//Act
		validator.validate(product);
		//Assert
		Assert.assertEquals(1, bindException.getErrorCount());
		Assert.assertTrue(bindException.getLocalizedMessage().contains("You cannot add more than 99 units if the unit price is greater than 1000."));
	}


	@Test
	public void a_valid_product_should_not_get_any_error_during_validation() {
		// Arrange
		Product product = new Product("P9876", "iPhone5s", new BigDecimal(1500));
		product.setCategory("Tablet");
		BindException bindException = new BindException(product, "product");
		//Act
		ValidationUtils.invokeValidator(unitsValidator, product, bindException);
		//Assert
		Assert.assertEquals(0, bindException.getErrorCount());
	}
}
